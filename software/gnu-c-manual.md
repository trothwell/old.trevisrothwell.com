---
layout: software
title: The GNU C Reference Manual
slug-title: One Article
---

<p><img src="https://www.gnu.org/software/gnu-c-manual/gnu-db-2.jpg" class="alignright" width="240" style="border: 0px">For the <a href="https://www.fsf.org/">Free Software
Foundation</a>, I have worked as primary author and maintainer of <a
href="https://www.gnu.org/software/gnu-c-manual/">The GNU C Reference
Manual</a>, known to have been a resource referenced for courses
at:</p>

<ul>
<li>Bucknell University</li>
<li>California Polytechnic State University</li>
<li>City University of New York</li>
<li>Cornell College</li>
<li>De La Salle University</li>
<li>Dickinson College</li>
<li>Federal University of Uberlândia</li>
<li>Florida International University</li>
<li>Georgia Southern University</li>
<li>Goucher College</li>
<li>Gustavus Adolphus College</li>
<li>Humboldt University of Berlin</li>
<li>James Madison University</li>
<li>Lafayette College</li>
<li>Louisiana State University</li>
<li>Milwaukee School of Engineering</li>
<li>National Chiao Tung University</li>
<li>New York University</li>
<li>Oregon State University</li>
<li>Queen’s University</li>
<li>Radboud University Nijmegen</li>
<li>Rhodes University</li>
<li>Rochester Institute of Technology</li>
<li>Ryerson University</li>
<li>San Jose State University</li>
<li>Sapienza University of Rome</li>
<li>Stanford University</li>
<li>Stony Brook University</li>
<li>University of Arizona</li>
<li>University of California at Berkeley</li>
<li>University of California at San Diego</li>
<li>University of California at Santa Cruz</li>
<li>University of Freiburg</li>
<li>University of Hawaii</li>
<li>University of Minnesota</li>
<li>University of North Florida</li>
<li>University of Oklahoma</li>
<li>University of Ontario Institute of Technology</li>
<li>University of Patras</li>
<li>University of Peloponnese</li>
<li>University of Puget Sound</li>
<li>University of Rhode Island</li>
<li>University of Rochester</li>
<li>University of San Francisco</li>
<li>University of Washington</li>
<li>University of Wisconsin-Superior</li>
<li>Wofford College</li>
<li>Østfold University College</li>
</ul>

<p>and also cited in:</p>

<ul>
<li><a href="http://www.cs.columbia.edu/~sedwards/classes/2015/4115-fall/lrms/Dice.pdf">The JFlat Reference Manual</a></li>
<li><a href="http://dl.acm.org/citation.cfm?id=2721956.2721959">Patterns for Hardware-Independent Development for Embedded Systems</a> (Proceedings of the 19th European Conference on Pattern Languages of Programs)</li>
</ul>

<p>Wow!</p>

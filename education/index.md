---
layout: section
title: Education
slug-title: One Section
---

<a href="https://www.flickr.com/photos/trothwell/6810039477" title="IMG_7354 by Trevis Rothwell, on Flickr"><img src="https://farm8.staticflickr.com/7162/6810039477_45bd1bf2e9_m.jpg" width="240" height="160" alt="IMG_7354" class="alignright"></a>

<blockquote>
You think you know when you learn,
are more sure when you can write,
even more when you can teach,
but certain when you can program.<br>
&mdash; Alan Perlis
</blockquote>

<p> When the <a
href="http://en.wikipedia.org/wiki/University_of_Bologna">University
of Bologna</a> opened its doors in 1088, centuries before the printing
press and a millennium before the internet, gathering dozens of
students into a room and talking at them was a great way to transmit
knowledge. Today there are lots of ways to to learn, but short of
entering the hallowed halls of academia and earning a degree, how can
we demonstrate to others what we have learned?

<p> Instead of being dependent upon paper credentials, perhaps what we
should aim to do is to write about what we have learned; teach others
as we have opportunity; and build and create things with what we know.

<h3>Creative Learning</h3>

<p> In the spring of 2013, I participated in an <a
href="http://www.media.mit.edu/">MIT Media Lab</a> course on
<i>Learning Creative Learning</i>. These are some essays I wrote as
assigned activities or voluntary reflections during the course.

<ul>
<li><a href="/education/abstractions">Abstractions</a></li>
<li><a href="/education/informal-learning">Informal Learning</a></li>
<li><a href="/education/programming-gears">Programming Gears</a></li>
<li><a href="http://tjr.posthaven.com/mindstorms-children-computers-and-powerful-ideas">Review
of <i>Mindstorms</i></a> (blog post)</li>
</ul>

<p> Here also is <a
href="http://www.flickr.com/photos/trothwell/8526901532/">my
"painting"</a> made with <a href="http://turtleart.org/">TurtleArt</a>
for the March 4 class session.

<h3>Book Reviews</h3>

<p> Trying to apply a "creative" component to routine book learning, I
started writing <a href="/book-reviews/">reviews / summaries of books
that I read</a>.
---
layout: page
title: Blog Posts by Category
slug-title: One listing
---

{% for category in site.categories %}
  <div class="archive-group">
    {% capture category_name %}{{ category | first }}{% endcapture %}
    <div id="#{{ category_name | slugize }}"></div>
    <p></p>

    <h2 class="category-head">{{ category_name }}</h2>
    <a name="{{ category_name | slugize }}"></a>
    <ul>
    {% for post in site.categories[category_name] %}
      <article class="archive-item">
        <li>
	  <a href="{{ site.baseurl }}{{ post.url }}">{{post.title}}</a>
	  <span class="post-date">({{ post.date | date_to_string }})</span>
	</li>
      </article>
    {% endfor %}
    </ul>
  </div>
{% endfor %}
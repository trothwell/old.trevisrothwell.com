---
layout: home
---
<a data-flickr-embed="true" href="https://www.flickr.com/photos/trothwell/35086730606/in/album-72157681671179654/" title="IMG_2213"><img src="https://live.staticflickr.com/4211/35086730606_9de9d74600_m.jpg" width="240" height="160" alt="IMG_2213" class="alignright"></a>Welcome to my home page on the
[World Wide Web](http://www.w3.org/History.html)!

I keep myself occupied developing [software](/software/), creating music, taking [photographs](/photography/), and caring for the planet's most adorable [border collie](/dogs/samantha).

I received a degree in computer science from [Cornell
College](https://www.cornellcollege.edu/) and have enjoyed some
academic refreshment at the [Massachusetts Institute of
Technology](https://web.mit.edu/iap/).

## What's New?

There might be something new on my [weblog](https://trevis.rothwell.blog/)...

## Elsewhere on the Web

* [Flickr photos](http://www.flickr.com/photos/trothwell/)

## Contact

Feel free to email me at [tjr@gnu.org](mailto:tjr@gnu.org) and to
encrypt your mail to me using my [OpenPGP-compliant public
key](http://pgp.mit.edu/pks/lookup?op=get&search=0x461537E136A12D8D).
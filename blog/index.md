---
layout: page
title: Blog Posts
slug-title: Blog
---

<ul>
  {% for post in site.posts %}
    <li>    
      <a href="{{ post.url }}">{{ post.title }}</a>
      <span class="post-date">({{ post.date | date_to_string }})</span>
    </li>
  {% endfor %}
</ul>

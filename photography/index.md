---
layout: section
title: Photography
---

<a href="https://www.flickr.com/photos/trothwell/5129954282/in/album-72157625150569999/" title="IMG_0274.JPG"><img src="https://live.staticflickr.com/4026/5129954282_f912e8664e_m.jpg" width="240" height="160" alt="IMG_0274.JPG" class="alignright"></a>

## Places

* [Joshua Tree National Park](joshua-tree-national-park)

## Camera Equipment

* [Cooking Chili with a Canon 100mm Macro Lens](cooking-chili)
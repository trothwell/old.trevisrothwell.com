---
layout: photography
title: Joshua Tree National Park
---

[Joshua Tree National Park](https://www.nps.gov/jotr/index.htm) is a
California desert area about 130 miles east of Los Angeles. The area
inside the park might not look that much different than the 50 miles
or so of desert outside the park, except for the striking presence of
[Joshua Trees](https://en.wikipedia.org/wiki/Yucca_brevifolia), or,
*yucca brevifolia*.

<center>
<a href="https://www.flickr.com/photos/trothwell/30905990337/in/album-72157673351330427/" title="IMG_3160"><img src="https://live.staticflickr.com/4841/30905990337_a468b0a71f_m.jpg" width="240" height="160" alt="IMG_3160" class="photo"></a>

<a href="https://www.flickr.com/photos/trothwell/44932728005/in/album-72157673351330427/" title="IMG_3152"><img src="https://live.staticflickr.com/4912/44932728005_5fe3643746_m.jpg" width="240" height="160" alt="IMG_3152" class="photo"></a>

<a href="https://www.flickr.com/photos/trothwell/31974476228/in/album-72157673351330427/" title="IMG_3162"><img src="https://live.staticflickr.com/1947/31974476228_6287509efd_m.jpg" width="240" height="160" alt="IMG_3162" class="photo"></a>
</center>

As with any photographic subject, lighting is important. It might be easy to
make your way out to the park at noonish, but the mid-day sunlight makes for
pretty flat, uninteresting pictures:

<center>
<a href="https://www.flickr.com/photos/trothwell/5113118694/in/album-72157625235270902/" title="IMG_4522.JPG"><img src="https://live.staticflickr.com/1205/5113118694_c13d69315e_m.jpg" width="240" height="160" alt="IMG_4522.JPG" class="photo"></a>

<a href="https://www.flickr.com/photos/trothwell/5112515589/in/album-72157625235270902/" title="IMG_4489.JPG"><img src="https://live.staticflickr.com/1217/5112515589_a4e1e093c3_m.jpg" width="240" height="160" alt="IMG_4489.JPG" class="photo"></a>

<a href="https://www.flickr.com/photos/trothwell/5112515927/in/album-72157625235270902/" title="IMG_4492.JPG"><img src="https://live.staticflickr.com/1260/5112515927_5477705959_m.jpg" width="240" height="160" alt="IMG_4492.JPG" class="photo"></a>
</center>

I would rather get some flat, uninteresting pictures of interesting subjects than get
no pictures at all, but even better would be to get out to the park early in the day
(before or just after sunrise) or late in the day (just before sunset):

<center>
<a href="https://www.flickr.com/photos/trothwell/45120878964/in/album-72157673351330427/" title="IMG_3157"><img src="https://live.staticflickr.com/4870/45120878964_cc94905c17_m.jpg" width="240" height="160" alt="IMG_3157" class="photo"></a>

<a href="https://www.flickr.com/photos/trothwell/45796252672/in/album-72157673351330427/" title="IMG_3168"><img src="https://live.staticflickr.com/4891/45796252672_2f21d7b559_m.jpg" width="160" height="240" alt="IMG_3168" class="photo"></a>

<a href="https://www.flickr.com/photos/trothwell/30905782977/in/album-72157673351330427/" title="IMG_3185"><img src="https://live.staticflickr.com/4840/30905782977_0fd80333e4_m.jpg" width="240" height="160" alt="IMG_3185" class="photo"></a>
</center>

As the sun sets, the darkening sky yields views of the trees and the park that look
much different than what you see in daylight:

<center>
<a href="https://www.flickr.com/photos/trothwell/45120387134/in/album-72157673351330427/" title="IMG_3327"><img src="https://live.staticflickr.com/4804/45120387134_40d35e69d4_m.jpg" width="240" height="160" alt="IMG_3327" class="photo"></a>

<a href="https://www.flickr.com/photos/trothwell/44028466540/in/album-72157673351330427/" title="IMG_3313"><img src="https://live.staticflickr.com/4913/44028466540_7fcf9f3b88_m.jpg" width="240" height="160" alt="IMG_3313" class="photo"></a>

<a href="https://www.flickr.com/photos/trothwell/45120392584/in/album-72157673351330427/" title="IMG_3319"><img src="https://live.staticflickr.com/4880/45120392584_da63237ffc_m.jpg" width="240" height="160" alt="IMG_3319" class="photo"></a>
</center>

That was about the end of the sunset picture-taking for me, as I did
not take a tripod, and with the final bit of sunlight gone, it was
hard to capture anything else. If you do take a tripod (and you
should, if you plan to stay past sunset), then you can get a whole new
category of Joshua Tree photos, including distant stars in the
background.

Watch your step, though! The park is home to a variety of creatures,
including tarantulas and scorpions, which tend to stay burrowed
underground during the day and come out at night. (I saw no animals
at all while on foot, but did see a coyote along the road as I
drove to the park exit.)
